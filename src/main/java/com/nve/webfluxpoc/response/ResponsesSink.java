package com.nve.webfluxpoc.response;

import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.cloud.stream.annotation.StreamListener;
import reactor.core.publisher.Flux;

@EnableBinding(Sink.class)
public class ResponsesSink {

    private Flux<String> responses;

    public Flux<String> listenFor(String correlationId) {
        return this.responses.filter(response -> response.equals(correlationId)).take(1);
    }

    @StreamListener(Sink.RESPONSES)
    public void init(Flux<String> responses) {
        this.responses = responses;
    }

}
