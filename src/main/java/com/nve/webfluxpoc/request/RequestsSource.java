package com.nve.webfluxpoc.request;

import org.springframework.cloud.stream.annotation.EnableBinding;
import org.springframework.kafka.support.KafkaHeaders;
import org.springframework.messaging.MessageChannel;
import org.springframework.messaging.support.MessageBuilder;

@EnableBinding(Source.class)
public class RequestsSource {

    private final MessageChannel source;

    public RequestsSource(Source source) {
        this.source = source.requests();
    }

    public void send(String message) {
        this.source.send(
                MessageBuilder
                        .withPayload(message)
                        .build()
        );
    }

}
