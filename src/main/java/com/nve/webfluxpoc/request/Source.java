package com.nve.webfluxpoc.request;

import org.springframework.cloud.stream.annotation.Output;
import org.springframework.messaging.MessageChannel;

public interface Source {

    String REQUESTS = "requests";

    @Output(REQUESTS)
    MessageChannel requests();

}
