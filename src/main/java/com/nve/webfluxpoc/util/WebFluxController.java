package com.nve.webfluxpoc.util;

import com.nve.webfluxpoc.request.RequestsSource;
import com.nve.webfluxpoc.response.ResponsesSink;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;

@RestController
public class WebFluxController {

    @Autowired
    private ResponsesSink responsesTest;

    @Autowired
    private RequestsSource requestsTest;

    @PostMapping(path = "/produce", consumes = MediaType.APPLICATION_JSON_VALUE)
    public Flux<String> writeToTopic() {
        Integer count = 1;
        Double correlationId = Math.random();
        this.requestsTest.send(correlationId.toString());
        Flux<String> value = responsesTest.listenFor(correlationId.toString()).take(1);

        return value;
    }
}
