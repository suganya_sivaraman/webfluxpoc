package com.nve.webfluxpoc.config;
import com.nve.webfluxpoc.request.RequestsSource;
import com.nve.webfluxpoc.request.Source;
import com.nve.webfluxpoc.response.ResponsesSink;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.AutoConfigureBefore;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class StreamConfig {

    @Bean
    public RequestsSource requestsTest(Source source) {
        return new RequestsSource(source);
    }

    @Bean
    public ResponsesSink responsesTest() {
        return new ResponsesSink();
    }
}
